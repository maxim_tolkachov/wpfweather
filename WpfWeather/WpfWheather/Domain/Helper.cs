﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfWheather.Domain
{
    class Helper
    {
        public static string GetEnumDescription(Enum value)
        {
            var type = value.GetType();
            var memInfo = type.GetMember(value.ToString());
            var atttributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute),
                false);
            var description = ((DescriptionAttribute)atttributes[0]).Description;
            return description;
        }
    }
}
