﻿using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace WpfWheather.Domain
{
    class WeatherDayModel
    {
        public string Name { get; set; }
        public string WeatherData { get; set; }
        public BitmapImage Icon { get; set; }

    }

    class WeatherDayModelPer : WeatherDayModel
    {
        public string Date { get; set; }
    }

    enum Forecasts
    {
        [Description("Current weather")]
        CurrentDay,
        [Description("Week(per 3h)")]
        CurrentWeek,
        [Description("16 days(per day)")]
        Daily
    };
}
