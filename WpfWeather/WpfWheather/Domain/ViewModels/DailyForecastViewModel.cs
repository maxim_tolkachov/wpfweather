﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfWheather.Domain
{
    class DailyForecastViewModel : INotifyPropertyChanged, IMapper 
    {
        private ObservableCollection<WeatherDayModel> days;
        private string location;
        private WeatherService _weatherService;

        public event PropertyChangedEventHandler PropertyChanged;

        public DailyForecastViewModel(dynamic json)
        {
            _weatherService = new WeatherService(); 
            days = new ObservableCollection<WeatherDayModel>();
            MapValues(json);
        }
        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
                OnPropertyChanged("Location");
            }
        }
        public ObservableCollection<WeatherDayModel> Days
        {
            get
            {
                return days;
            }
            set
            {
                days = value;
                OnPropertyChanged("Days");
            }
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public void MapValues(dynamic json)
        {
            var index = 0;
            Location = $"Weather in {json.city.name}, {json.city.country}";
            foreach(var item in json.list)
            {
                var weekDay = new WeatherDayModelPer();
                weekDay.Date = GetDateFromUnix((double)item.dt).ToShortDateString();
                weekDay.Icon = _weatherService.GetIcon((string)item.weather[0].icon);
                weekDay.WeatherData = $"Temperature : {Math.Round((decimal)item.temp.min - 273, 2)} - {Math.Round((decimal)item.temp.max - 273, 2)} °C, "
                + $"Humidity: {item.humidity} %, Wind: {item.speed} m/sec," + Environment.NewLine + $"Clouds: {item.clouds} %, Pressure : {item.pressure} hpa, {item.weather[0].description }";
                Days.Insert(index++, weekDay);
                
            }
        }

        private DateTime GetDateFromUnix(double unix)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dtDateTime.AddSeconds(unix);
        }
    }
}
