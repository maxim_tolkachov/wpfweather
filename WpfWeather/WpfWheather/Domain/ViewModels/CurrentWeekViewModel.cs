﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfWheather.Domain
{
    class CurrentWeekViewModel : INotifyPropertyChanged, IMapper
    {
        private ObservableCollection<WeatherDayModel> week;
        private string location;
        private WeatherService _weatherService;
        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
                OnPropertyChanged("Locatoin");
            }
        }

        public ObservableCollection<WeatherDayModel> Week
        {
            get
            {
                return week;
            }
            set
            {
                week = value;
                OnPropertyChanged("Week");
            }
        }
        public CurrentWeekViewModel(dynamic json)
        {
            week = new ObservableCollection<WeatherDayModel>();
            _weatherService = new WeatherService();
            MapValues(json);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
        public void MapValues(dynamic json)
        {
            var index = 0;
            Location = $" Weather in {json.city.name}, {json.city.country}";
            
            foreach(var item in json.list)
            {
                var weekDay = new WeatherDayModelPer();
                weekDay.Icon = _weatherService.GetIcon((string)item.weather[0].icon);
                weekDay.WeatherData = $"Temperature : {Math.Round((decimal)item.main.temp - 273, 2)} °C, Clouds: {item.clouds.all} %, Pressure : {item.main.pressure} hpa," 
                    + Environment.NewLine 
                    + $"Wind : {item.wind.speed} m/sec," + Environment.NewLine + $"{item.weather[0].description }";
                weekDay.Date = ((DateTime)item.dt_txt).ToString("MM.dd.yy hh:mm");
                Week.Insert(index++, weekDay);
            }

        }
    }
}
