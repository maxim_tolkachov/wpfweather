﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfWheather.Domain
{
    class CurrentDayViewModel : INotifyPropertyChanged, IMapper
    {
        private WeatherDayModel currentDay;
        public event PropertyChangedEventHandler PropertyChanged;
        private WeatherService _weatherService;
        public CurrentDayViewModel(dynamic json)
        {
            _weatherService = new WeatherService();
            MapValues(json);
        }
        
        public WeatherDayModel CurrentDay
        {
            get
            {
                return currentDay;
            }
            set
            {
                currentDay = value;
                OnPropertyChanged("CurrentDay");
            }
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public void MapValues(dynamic json)
        {
            currentDay = new WeatherDayModel();
            currentDay.Icon = _weatherService.GetIcon((string)json.weather[0].icon);
            currentDay.WeatherData = $" Temperature : {Math.Round((decimal)json.main.temp - 273, 2)} °C" + Environment.NewLine
               + $" Pressure : {json.main.pressure} hpa" + Environment.NewLine
               + $" Cloudliness : {json.clouds.all} %" + Environment.NewLine
               + $" Wind : {json.wind.speed} m/s" + Environment.NewLine
               + $" Humidity : {json.main.humidity} %" + Environment.NewLine
               + $" {json.weather[0].description}";
            currentDay.Name = $"Weather in {json.name}, {json.sys.country} at " + DateTime.Now.ToShortDateString();
        }
    }
}
