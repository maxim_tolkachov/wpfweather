﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfWheather.Domain
{
    class MainViewModel : INotifyPropertyChanged
    {
        private City selectedCity;
        private string selectedForecast;
        public ObservableCollection<City> Cities { get; set; }
        private WeatherService _wheatherService;
        public City SelectedCity
        {
            get
            {
                return selectedCity;
            }
            set
            {
                selectedCity = value;
                OnPropertyChanged("SelectedCity");
            }
        }

        public string SelectedForecast
        {
            get
            {
                return selectedForecast;
            }
            set
            {
                selectedForecast = value;
                OnPropertyChanged("SelectedForecast");
            }
        }

        public MainViewModel()
        {
            Cities = Deserialize();
            _wheatherService = new WeatherService();
        }
        private Command addCommand;
        public Command AddCommand
        {
            get
            {
                return addCommand ??
                  (addCommand = new Command(async obj => 
                  {
                      var cityName = obj as string;

                      if(!string.IsNullOrWhiteSpace(cityName))
                      {
                          if (!Cities.Select(i => i.Name.ToLower()).Contains(cityName.ToLower()))
                          {
                              var json = await _wheatherService.GetDataForCity(cityName);
                              if (json != null)
                              {
                                  var city = new City
                                  {
                                      Name = obj as string,
                                      Id = json.city.id
                                  };
                                  Cities.Insert(0, city);
                                  SelectedCity = city;
                                  Serialize();
                              }
                              else
                              {
                                  MessageBox.Show("Enter a valid city");
                              }
                          }
                          else
                          {
                              MessageBox.Show("Already exists");
                          }
                      }
                      else
                      {
                          MessageBox.Show("Enter a valid city");
                      }
                      
                  }));
            }
        }

        private Command removeCommand;
        public Command RemoveCommand
        {
            get
            {
                return removeCommand ??
                    (removeCommand = new Command(obj =>
                    {
                        var city = obj as City;
                        if (city != null)
                        {
                            Cities.Remove(city);
                            Serialize();
                        }
                    },
                    (obj) => Cities.Count > 0));
            }
        }
        
        private Command openDetail;
        public Command OpenDetail
        {
            get
            {
                return openDetail ??
                    (openDetail = new Command(async obj =>
                    {
                        var city = obj as City; 
                        if(city != null)
                        {
                            var forecast = SelectedForecast;
                            if (forecast != null)
                            {
                                if (String.Equals(forecast,Helper.GetEnumDescription(Forecasts.CurrentDay)))
                                {
                                    var json = await _wheatherService.GetWheatherById(city.Id);
                                    var cdw = new CurrentDayWindow();
                                    cdw.DataContext = new CurrentDayViewModel(json); ;
                                    cdw.ShowDialog();
                                }
                                else if (String.Equals(forecast, Helper.GetEnumDescription(Forecasts.CurrentWeek)))
                                {
                                    var json = await _wheatherService.GetForecastById(city.Id);
                                    var ww = new WeekWindow();
                                    ww.DataContext = new CurrentWeekViewModel(json);
                                    ww.ShowDialog();
                                }
                                else if (String.Equals(forecast, Helper.GetEnumDescription(Forecasts.Daily)))
                                {
                                    var json = await _wheatherService.GetForecastDailyById(city.Id);
                                    var dfw = new DailyForecastWindow();
                                    dfw.DataContext = new DailyForecastViewModel(json);
                                    dfw.ShowDialog();
                                }
                            }
                            else
                            {
                                MessageBox.Show("Choose forecast");
                            }

                        }
                        else
                        {
                            MessageBox.Show("Choose a city");
                        }
                    }));
            }
        }

        private void Serialize()
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream("cities.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, Cities);
            }
        }

        private ObservableCollection<City> Deserialize()
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream("cities.dat", FileMode.OpenOrCreate))
            {
                try
                {
                    var result = (ObservableCollection<City>)formatter.Deserialize(fs);
                    if (result != null && result.Count > 0)
                    {
                        return result;
                    }
                    else
                    {
                        return new ObservableCollection<City>();
                    }
                }
                catch(Exception ex)
                {
                    return new ObservableCollection<City>();
                }
                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
