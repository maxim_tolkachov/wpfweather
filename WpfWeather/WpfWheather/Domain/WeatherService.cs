﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace WpfWheather.Domain
{
    class WeatherService 
    {
        private readonly string KeyPart = "&APPID=c71fac6d157b558f7d4be2221922ae1d";
        private WebClient wc = new WebClient();
            
        public async Task<dynamic> GetDataForCity(string city)
        {
            if(!String.IsNullOrEmpty(city))
            {
                try
                {
                    var data = await wc.DownloadStringTaskAsync(new Uri("http://api.openweathermap.org/data/2.5/forecast?q=" + city + "," + KeyPart));
                    dynamic json = JsonConvert.DeserializeObject<dynamic>(data);
                    return json;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        public async Task<dynamic> GetWheatherById(int cityId)
        {
            var data = await wc.DownloadStringTaskAsync(new Uri("http://api.openweathermap.org/data/2.5/weather?id=" + cityId + "," + KeyPart));
            dynamic json = JsonConvert.DeserializeObject<dynamic>(data);
            return json;
        }

        public async Task<dynamic> GetForecastById(int cityId)
        {
            var data = await wc.DownloadStringTaskAsync(new Uri("http://api.openweathermap.org/data/2.5/forecast?id=" + cityId + "," + KeyPart));
            dynamic json = JsonConvert.DeserializeObject<dynamic>(data);
            return json;
        }

        public async Task<dynamic> GetForecastDailyById(int cityId)
        {
            var data = await wc.DownloadStringTaskAsync(new Uri("http://api.openweathermap.org/data/2.5/forecast/daily?id=" + cityId + "&cnt=16" + KeyPart));
            dynamic json = JsonConvert.DeserializeObject<dynamic>(data);
            return json;
        }

        public BitmapImage GetIcon(string icon)
        {
            var img = new BitmapImage(new Uri("http://api.openweathermap.org/img/w/" + icon + ".png"));
            
            return img;
        }
    }
}
