﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfWheather.Domain
{
    interface IMapper
    {
        void MapValues(dynamic json);
    }
}
